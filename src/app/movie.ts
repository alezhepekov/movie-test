export class Movie {

  constructor() {
    this.id = null;
    this.Title = null;
    this.Year = null;
    this.imdbID = null;
    this.Type = null;
    this.Poster = null;
  }

  id: number;
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
}
