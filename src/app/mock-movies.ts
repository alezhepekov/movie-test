import { Movie } from './movie';

export const MOVIES: Movie[] = [
  {
    id: 1,
    Title: 'A.A.A. Masseuse, Good-Looking, Offers Her Services',
    Year: '1972',
    imdbID: 'tt0068162',
    Type: 'movie',
    Poster: 'https://m.media-amazon.com/images/M/MV5BMTU3ZDU2ZjAtMDQ4NS00ZDA4LTkwMmItNmI5M2NiMjZjYWIyXkEyXkFqcGdeQXVyMTYxNjkxOQ@@._V1_SX300.jpg'
  },
  {
    id: 2,
    Title: 'A.A.A. Achille',
    Year: '2003',
    imdbID: 'tt0264330',
    Type: 'movie',
    Poster: 'https://m.media-amazon.com/images/M/MV5BNDFmZDc1MzMtNzY5My00YjY4LTg4NTMtZTE5ZWMxYTllYzI1XkEyXkFqcGdeQXVyMDk2Mzc2MA@@._V1_SX300.jpg'
  },
  {
    id: 3,
    Title: 'AAA, la película: Sin límite en el tiempo',
    Year: '2010',
    imdbID: 'tt1484065',
    Type: 'movie',
    Poster: 'https://m.media-amazon.com/images/M/MV5BMTgzMjE4MDcwM15BMl5BanBnXkFtZTcwNzc2ODMxMw@@._V1_SX300.jpg'
  },
  {
    id: 4,
    Title: 'Las aAA son las tres armas',
    Year: '1979',
    imdbID: 'tt0307911',
    Type: 'movie',
    Poster: 'https://m.media-amazon.com/images/M/MV5BOTZkY2ZjZGUtYjMwOC00OTM0LWEwYmUtOGJjNjk4NTVmMjliXkEyXkFqcGdeQXVyMjQ0NzgwNzY@._V1_SX300.jpg'
  },
  {
    id: 5,
    Title: 'AAA Championships at Fartown, Huddersfield',
    Year: '1901',
    imdbID: 'tt1669078',
    Type: 'movie',
    Poster: 'https://m.media-amazon.com/images/M/MV5BMTMwNjcwMjQ2Ml5BMl5BanBnXkFtZTcwMjc2OTY1Mw@@._V1_SX300.jpg'
  },
  {
    id: 6,
    Title: 'AAA When Worlds Collide',
    Year: '1994',
    imdbID: 'tt0356319',
    Type: 'movie',
    Poster: 'https://m.media-amazon.com/images/M/MV5BZWQ1YzQ5NzMtNGQ4MS00MTRhLWEwNjItNzdkODViOTViY2UxL2ltYWdlXkEyXkFqcGdeQXVyMzIxNTI3MTE@._V1_SX300.jpg'
  },
  {
    id: 7,
    Title: 'Aaa Re Saathi Aa',
    Year: '2009',
    imdbID: 'tt1459820',
    Type: 'movie',
    Poster: 'N/A'
  },
  {
    id: 8,
    Title: 'AAA: Rey de reyes',
    Year: '2008',
    imdbID: 'tt1610295',
    Type: 'movie',
    Poster: 'N/A'
  },
  {
    id: 9,
    Title: 'Crocotires Traction AAA',
    Year: '2000',
    imdbID: 'tt0286565',
    Type: 'movie',
    Poster: 'N/A'
  },
  {
    id: 10,
    Title: 'AAA agenzia abbandoni',
    Year: '1999',
    imdbID: 'tt0357409',
    Type: 'movie',
    Poster: 'N/A'
  }
];
